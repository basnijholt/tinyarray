#!/usr/bin/env python

# Copyright 2012-2015 Tinyarray authors.
#
# This file is part of Tinyarray.  It is subject to the license terms in the
# file LICENSE.rst found in the top-level directory of this distribution and
# at https://gitlab.kwant-project.org/kwant/tinyarray/blob/master/LICENSE.rst.
# A list of Tinyarray authors can be found in the README.rst file at the
# top-level directory of this distribution and at
# https://gitlab.kwant-project.org/kwant/tinyarray.

import os
from setuptools import setup, Extension
import versioneer

README_FILE = 'README.rst'
VERSION_HEADER = ['src', 'version.hh']

CLASSIFIERS = """\
Development Status :: 5 - Production/Stable
Intended Audience :: Science/Research
Intended Audience :: Developers
License :: OSI Approved :: BSD License
Programming Language :: Python :: 2
Programming Language :: Python :: 3
Programming Language :: C++
Topic :: Software Development
Topic :: Scientific/Engineering
Operating System :: POSIX
Operating System :: Unix
Operating System :: MacOS
Operating System :: Microsoft :: Windows"""

distr_root = os.path.dirname(os.path.abspath(__file__))
version = versioneer.get_version()

def long_description():
    text = []
    skip = True
    try:
        with open(README_FILE) as f:
            for line in f:
                if line == "\n":
                    if skip:
                        skip = False
                        continue
                    elif text[-1] == '\n':
                        text.pop()
                        break
                if not skip:
                    text.append(line)
    except:
        return ''
    text[-1] = text[-1].rstrip()
    return ''.join(text)

module = Extension('tinyarray',
                   language='c++',
                   sources=['src/arithmetic.cc', 'src/array.cc',
                            'src/functions.cc'],
                   depends=['src/arithmetic.hh', 'src/array.hh',
                            'src/conversion.hh', 'src/functions.hh'])

        
def main():
    setup(name='tinyarray',
          version=version,
          author='Christoph Groth (CEA) and others',
          author_email='christoph.groth@cea.fr',
          description="Arrays of numbers for Python, optimized for small sizes",
          long_description=long_description(),
          url="https://gitlab.kwant-project.org/kwant/tinyarray",
          download_url="http://downloads.kwant-project.org/tinyarray/",
          license="Simplified BSD license",
          platforms=["Unix", "Linux", "Mac OS-X", "Windows"],
          classifiers=CLASSIFIERS.split('\n'),
          cmdclass=versioneer.get_cmdclass(),
          ext_modules=[module],
          test_suite = 'nose.collector',
          setup_requires=['nose >= 1.0'])


if __name__ == '__main__':
    main()
